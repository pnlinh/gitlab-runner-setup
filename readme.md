# Gitlab Runner Setup

Quick start gitlab runners in seconds.

## Get started

- Clone this repository
- Run `docker-compose up -d` to start engine
- Run `CI_SERVER_URL=https://gitlab.com/ REGISTRATION_TOKEN=your_runner_token ./register.sh` to register runner

Bingo!!! Your runner is already!
