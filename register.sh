#!/bin/bash

set -e

if [[ -z "${CI_SERVER_URL}" || -z "${REGISTRATION_TOKEN}" ]]; then
    echo "please suply CI_SERVER_URL & REGISTRATION_TOKEN, you can find them in gitlab settings > CI/CD page" >&2
    exit 1
fi

docker run --rm \
    -e CI_SERVER_URL=${CI_SERVER_URL} \
    -e REGISTRATION_TOKEN=${REGISTRATION_TOKEN} \
    -v "$(pwd)/gitlab/runner:/etc/gitlab-runner:Z" \
    gitlab/gitlab-runner:alpine \
        register \
        --docker-privileged \
        --non-interactive \
        --locked=false \
        --name=atm-m1 \
        --executor=docker \
        --docker-image=bot94/docker-ext:1.0 \
        --docker-volumes=/var/run/docker.sock:/var/run/docker.sock
